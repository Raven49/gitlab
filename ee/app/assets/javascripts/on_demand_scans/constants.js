import { helpPagePath } from '~/helpers/help_page_helper';

export const HELP_PAGE_PATH = helpPagePath('user/application_security/dast/index', {
  anchor: 'on-demand-scans',
});
